<?php

class amazonSearchScrapper extends simpleScrapper
{

    /**
     * @var string
     */
    private $url = 'http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=';

    /**
     * Map of requested scrapped data and key for saving
     * @var array
     */
    private $scrapMap = [
        'title' => ['.s-access-title:first', 'text'],
        'price' => ['.s-price:first', 'text'],
        'img' => ["img[alt='Product Details']:first", 'attr', 'src'],
        'description' => ["div[class='a-column a-span5 a-span-last'] > div[class='a-row a-spacing-mini']:second > span[class='a-size-small a-color-secondary']:first", 'text']
    ];

    /**
     * @param $keyword
     */
    public function __construct($keyword)
    {
        $this->setUrl($keyword);
    }

    /**
     * @param $keyword
     */
    private function setUrl($keyword)
    {
        $this->url = 'http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=' . $keyword;
    }

    /**
     * Get by some html target
     * @param string $target
     * @return array
     */
    public function getByTarget($target = '#result_0')
    {

        /**
         * send url and get html from response
         */
        $html = $this->getHtmlFromUrl($this->url);

        /**
         * get phpQuery from Html
         */
        $phpQuery = $this->getPhpQueryDocument($html);

        /**
         * Scrap things by ScrapMap
         */
        return [
            'item' => $this->scrapByMap($phpQuery[$target]),
            'url' => $this->url
        ];

    }

    /**
     * @param $target
     * @return array
     */
    private function scrapByMap($target)
    {
        $result = [];
        foreach ($this->scrapMap as $key => $item) {

            if ($item[1] == "text") {
                $result[$key] = $this->getTextFromElement($target[$item[0]]);
            }

            if ($item[1] == "attr") {
                $result[$key] = $this->getAttrFromElement($target[$item[0]], $item[2]);
            }

        }
        return $result;
    }

}
