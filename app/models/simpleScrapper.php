<?php

class simpleScrapper
{
    /**
     * @param $type
     * @param $keyword
     * @return mixed
     * @throws Exception
     */
    public static function build($type, $keyword)
    {
        if (class_exists($type .= 'Scrapper')) {
            return new $type($keyword);
        } else {
            throw new Exception("Invalid Scrapper.");
        }
    }

    /**
     * Get Html from url
     * Simulate browser headers
     * @param $url
     * @return string
     */
    protected function getHtmlFromUrl($url)
    {

        $opts = array('http' =>
            array(
                'method' => 'GET',
                'user_agent ' => "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2) Gecko/20100301 Ubuntu/9.10 (karmic) Firefox/3.6",
                'header' => array(
                    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*;q=0.8'
                ),
            )
        );

        $context = stream_context_create($opts);

        return file_get_contents($url, false, $context);
    }

    /**
     * @param $html
     * @return phpQueryObject|QueryTemplatesParse|QueryTemplatesSource|QueryTemplatesSourceQuery
     */
    protected function getPhpQueryDocument($html)
    {
        return phpQuery::newDocumentPHP($html);
    }

    /**
     * @param $element
     * @return mixed
     */
    protected function getTextFromElement($element)
    {
        return ($text = $element->text()) ? $text : "NA";
    }

    /**
     * @param $element
     * @param $attrName
     * @return mixed
     */
    protected function getAttrFromElement($element, $attrName)
    {
        return ($attr=$element->attr($attrName)) ? $attr : "NA" ;
    }


}