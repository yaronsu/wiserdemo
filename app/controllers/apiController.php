<?php

class apiController extends \BaseController
{

    /**
     * Init New Rest Action
     *
     * @return Response
     */
    public function index()
    {

        /**
         * get Keyword as $_GET var
         */
        $keyword=Input::get('keyword');

        if(!$keyword){
            $response = [
                'response'=>'keyword is missing',
                'status'=>500
            ];
        }else{
            $response = [
                'response'=>simpleScrapper::build('amazonSearch', $keyword)->getByTarget(),
                'status'=>200
                ];
        }

        /**
         * Return Json Response to Client
         */
        return Response::json(['response' => $response['response']],$response['status'],[],JSON_PRETTY_PRINT);

    }


}
