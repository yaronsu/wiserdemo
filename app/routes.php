<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Show Err Message
 */
App::missing(function($exception)
{
    return Response::json(['response' => "The only route in this app is: /api/search?keyword=ps4"],500,[],JSON_PRETTY_PRINT);
});

/**
 * catch get Method of this url
 */
Route::get('/api/search','apiController@index');